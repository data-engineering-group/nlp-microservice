from fastapi import FastAPI
import spacy
from prometheus_fastapi_instrumentator import Instrumentator
from pydantic import BaseModel, Field

nlp = spacy.load("en_core_web_sm")
app = FastAPI()

Instrumentator().instrument(app).expose(app)


class Input(BaseModel):
    text: str = Field(..., min_length=10,
                      description='Text Input')


@app.get("/")
async def root():
    """Return hello world response."""
    return {"message": "Hello World"}


@app.post("/entities")
def get_entities(text_input: Input) -> dict:
    """Return list of entities contained in text.
    Params:
        text_input (Input):
    Return:
        output (dict):
    """
    doc = nlp(text_input.text)
    response_array = []
    for token in doc.ents:
        response = {
            "Text": token.text,
            "Lemma": token.lemma_,
            "Start Char": token.start_char,
            "End Char": token.end_char,
            "Label": token.label_,
        }
        response_array.append(response)
    return {"response": response_array}
