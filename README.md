## Name
NLP-Service

## Description
This project leverages spacy and fastapi to generate an entity extraction microservice.

## Usage
Use this project to gather entities from text or as a base image for fastapi and machine learning microservices.

## Capabilities
- docker
- fastapi
- grafana
- prometheus
- spacy
- terraform
